package az.ingress.demo;

import az.ingress.demo.model.Account;
import az.ingress.demo.model.Phone;
import az.ingress.demo.model.Student;
import az.ingress.demo.repository.AccountRepository;
import az.ingress.demo.repository.StudentRepository;
import az.ingress.demo.service.TransferService;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableCaching
public class SpringDemoMs19Application implements CommandLineRunner {

    private final AccountRepository accountRepository;
    private final TransferService transferService;
    private final StudentRepository studentRepository;
    private final EntityManagerFactory emf;
    private final Map<Integer, Account> map = new HashMap<>();
    private final Map<Integer, Phone> PphoneMap = new HashMap<>();

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoMs19Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception { //persistence context
//        int size = CacheManager.ALL_CACHE_MANAGERS.get(0)
//                .getCache("az.ingress.demo.model.Student").getSize();
//        log.info("size of cache is {}" , size);
        var em = emf.createEntityManager().unwrap(Session.class);
        em.find(Student.class,1);
//        size = CacheManager.ALL_CACHE_MANAGERS.get(0)
//                .getCache("az.ingress.demo.model.Student").getSize();
//        log.info("size of cache is {}" , size);
        var em2 = emf.createEntityManager().unwrap(Session.class);;
        em2.find(Student.class,1);
    }

    public Account getAccount(Integer id) {
        Account account = map.get(id);
        if (account == null) {
            account = accountRepository.findById(1).orElseThrow(RuntimeException::new);
            map.put(account.getId(), account);
            return account;
        } else {
            return account;
        }
    }
}
