package az.ingress.demo.service;

import az.ingress.demo.config.Config;
import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.mapper.StudentMapper;
import az.ingress.demo.model.Student;
import az.ingress.demo.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final Config config;
    private final StudentMapper studentMapper;

    public StudentServiceImpl(StudentRepository studentRepository, Config config, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.config = config;
        this.studentMapper = studentMapper;
    }

    @Override
    @Cacheable(key = "#id", cacheNames = "student")
    public StudentDto get(Integer id) {
        System.out.println(config.getList());
        log.info("Student service get method is working");
        Student student = studentRepository.findById(id).orElseThrow(() -> new RuntimeException("Student not found"));
        return studentMapper.entityToDto(student);
    }

    @Override
    public StudentDto create(StudentDto studentDto) {
        log.info("Student service create method is working");
        Student student = studentMapper.dtoToEntity(studentDto);
        studentRepository.save(student);

        return studentMapper.entityToDto(student);
    }

    @Override
    @CachePut(key = "#id", cacheNames = "student")
    public StudentDto update(Integer id, Student student) {
        log.info("Student service update method is working");
        Student entity = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setLastname(student.getLastname());
        entity = studentRepository.save(entity);
        return studentMapper.entityToDto(student);
    }

    @Override
    @CacheEvict(key = "#id", cacheNames = "student")
    public void delete(Integer id) {
        log.info("Student service delete method is working");
        studentRepository.deleteById(id);
    }
}
