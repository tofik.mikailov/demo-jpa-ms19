package az.ingress.demo.service;

import az.ingress.demo.model.Account;
import az.ingress.demo.repository.AccountRepository;
import jakarta.persistence.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory emf;
    private final ApplicationContext context;

    @SneakyThrows
    public void transfer(Account from, Account to, double amount) { //240 //330
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            if (from.getBalance() < amount) {
                throw new RuntimeException();
            }
            from.setBalance(from.getBalance() - amount);
            to.setBalance(to.getBalance() + amount);

            entityManager.merge(from);
            if (true) {
                throw new RuntimeException();
            }
            entityManager.merge(to);
            entityManager.getTransaction().commit();
        } catch (RuntimeException e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void transfer2(Account from, Account to, double amount, EntityManager entityManager) { //240 //330
        if (from.getBalance() < amount) {
            throw new RuntimeException();
        }
        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);
        entityManager.merge(from);
        if (true) {
            throw new RuntimeException();
        }
        entityManager.merge(to);
    }

    public void hello() {

    }

    @Transactional
    public void proxyTransfer(Account from, Account to, double amount) {
        //begin
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        hello();
        try {
            transfer2(from, to, amount, entityManager);
            entityManager.getTransaction().commit();
        } catch (NullPointerException e) {
            entityManager.getTransaction().commit();
        } catch (RuntimeException e) {
            entityManager.getTransaction().rollback();
        } catch (Exception e) {
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

        //commit
    }
}
