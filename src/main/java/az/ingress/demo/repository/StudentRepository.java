package az.ingress.demo.repository;

import az.ingress.demo.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Integer> {

//    @Query(value = "select * from student where age = :age", nativeQuery = true)
//    List<Student> findStudentWithAgeEquals(Integer age);

    //JPQL - Java persistence query language
    @Query(value = "from Student s where s.age = :age")
    List<Student> findStudentWithAgeEquals(Integer age);

    @Query(value = "from Student s join fetch s.phoneList")
    List<Student> findStudents();

    Optional<Student> findByName(String name);

//    @Query(value = "select age, count(*) as count from student group by age", nativeQuery = true)
//    List<StudentGroupByDto> groupByAge();

    @Query(value = "select new az.ingress.demo.repository.StudentGroupByDto(s.age, count(*)) from Student s group by s.age")
    List<StudentGroupByDto> groupByAgeJpql();

//    @Query("select s from Student s left join fetch s.phoneList")
//    @EntityGraph(value = "students_with_phone_list", type = EntityGraph.EntityGraphType.FETCH)
    @EntityGraph(attributePaths = {"phoneList"}, type = EntityGraph.EntityGraphType.FETCH)
    List<Student> findAllBy();

    @Override
    @EntityGraph(attributePaths = "phoneList")
    Optional<Student> findById(Integer integer);
}
